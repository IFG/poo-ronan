package Principal;

import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.MaskFormatter;

public class TelaPrincipal extends JFrame {

    private JPanel containerPrincipal;
    private JPanel containerSexo;
    private JPanel containerTabela;
    private JTextField campoNome;
    private JTextField campoDataNascimento;
    private JRadioButton buttonMasc;
    private JRadioButton buttonFem;
    private JComboBox campoTipo;
    private JLabel lblNome;
    private JLabel lblDataNascimento;
    private JLabel lblTipo;
    private JButton botaoSalvar;
    private JButton botaoExcluir;
    private JButton botaoCancelar;
    private JTable tblPessoas;
    private JScrollPane scrlPessoas;
    private List<Pessoa> listaDePessoas;

    private final int ALTURA_COMPONENTE = 30;
    private final int LARGURA_COMPONENTE_TEXTO = 300;
    private final int COORDENADA_MAIS_A_ESQUERDA = 30;

    public TelaPrincipal() throws ParseException, IOException {
        //Cria o arquivo se não existir
        File fw = new File("pooi-2019.txt");
        if (!fw.exists()) {
            new FileWriter("pooi-2019.txt");
        }

        listaDePessoas = new ArrayList<Pessoa>();
        listaDePessoas = buscaDadosDaTabela();

        inicializaTela();
        defineTabelaPessoas(listaDePessoas);
    }

    private void inicializaTela() throws ParseException {
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTitle("CRUD");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setSize(670, 480);
        setLocation(0, 0);

        containerPrincipal = new JPanel();
        containerPrincipal.setLayout(null);
        containerPrincipal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CRUD", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        defineComponentesNaTela();

        botaoSalvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (campoNome.getText().trim().length() == 0) {
                    JOptionPane.showMessageDialog(null, "Preencha o Nome!", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }

                try {
                    if (!dataValida(campoDataNascimento.getText().trim())) {
                        JOptionPane.showMessageDialog(null, "Preencha a Data Corretamente!", "Aviso", JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }

                String sexo = null;
                if (buttonMasc.isSelected()) {
                    sexo = "M";
                } else {
                    sexo = "F";
                }

                salvaRegistro(new Pessoa(campoNome.getText().trim(), campoDataNascimento.getText(), sexo, (String) campoTipo.getSelectedItem()), tblPessoas.getSelectedRow());
            }
        });

        botaoExcluir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                excluiRegistro(tblPessoas.getSelectedRow());
            }
        });

        botaoCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //Limpa os Campos
                    campoNome.setText(null);
                    campoDataNascimento.setText(null);
                    buttonMasc.setSelected(true);
                    campoTipo.setSelectedIndex(0);

                    //Recria a tabela para tirar seleção da linha
                    containerTabela.remove(scrlPessoas);
                    containerPrincipal.remove(containerTabela);
                    defineTabelaPessoas(listaDePessoas);
                } catch (ParseException ex) {
                    Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        add(containerPrincipal);
        setVisible(true);
    }

    private void defineComponentesNaTela() throws ParseException {
        containerPrincipal.removeAll();
        lblNome = new JLabel("Nome");
        lblNome.setBounds(30, 10, 50, 30);
        containerPrincipal.add(lblNome);

        campoNome = new JTextField();
        campoNome.setBounds(COORDENADA_MAIS_A_ESQUERDA, 35, LARGURA_COMPONENTE_TEXTO, ALTURA_COMPONENTE);
        containerPrincipal.add(campoNome);

        lblDataNascimento = new JLabel("Data de Nascimento");
        lblDataNascimento.setBounds(355, 10, 200, 30);
        containerPrincipal.add(lblDataNascimento);

        campoDataNascimento = new JFormattedTextField(new MaskFormatter("##/##/####"));
        campoDataNascimento.setBounds(355, 35, 170, ALTURA_COMPONENTE);
        containerPrincipal.add(campoDataNascimento);

        containerSexo = new JPanel();
        containerSexo.setLayout(null);

        buttonMasc = new JRadioButton("Masculino");
        buttonMasc.setLocation(15, 20);
        buttonMasc.setSize(85, 30);
        buttonMasc.setSelected(true);
        containerSexo.add(buttonMasc);

        buttonFem = new JRadioButton("Feminino");
        buttonFem.setLocation(135, 20);
        buttonFem.setSize(100, 30);
        containerSexo.add(buttonFem);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(buttonMasc);
        buttonGroup.add(buttonFem);
        containerPrincipal.add(containerSexo);
        containerSexo.setVisible(true);
        containerSexo.setSize(240, 60);
        containerSexo.setLocation(30, 70);
        containerSexo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sexo", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        lblTipo = new JLabel("Tipo");
        lblTipo.setBounds(355, 70, 200, 30);
        containerPrincipal.add(lblTipo);

        campoTipo = new JComboBox();
        campoTipo.setBounds(355, 95, 200, 30);
        campoTipo.addItem("SIMPLES");
        campoTipo.addItem("ESPECIAL");
        containerPrincipal.add(campoTipo);

        botaoSalvar = new JButton("Salvar");
        botaoSalvar.setBounds(30, 165, 100, ALTURA_COMPONENTE);
        containerPrincipal.add(botaoSalvar);

        botaoExcluir = new JButton("Excluir");
        botaoExcluir.setBounds(140, 165, 100, ALTURA_COMPONENTE);
        containerPrincipal.add(botaoExcluir);

        botaoCancelar = new JButton("Cancelar");
        botaoCancelar.setBounds(250, 165, 100, ALTURA_COMPONENTE);
        containerPrincipal.add(botaoCancelar);

        JPanel pnlDadosPessoais = new JPanel();
        pnlDadosPessoais.setBorder(BorderFactory.createTitledBorder("Dados Pessoais"));
        containerPrincipal.add(pnlDadosPessoais);
    }

    private List<Pessoa> buscaDadosDaTabela() throws ParseException, IOException {
        try {
            listaDePessoas = new ArrayList<Pessoa>();
            File arquivoParaLeitura = new File("pooi-2019.txt");
            FileInputStream inputStream = new FileInputStream(arquivoParaLeitura);
            Scanner s = new Scanner(inputStream);

            while (s.hasNext()) {
                String array[] = new String[4];
                String linha = s.nextLine();
                array = linha.split(";");

                listaDePessoas.add(new Pessoa(array[0], array[1], array[2], array[3]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return listaDePessoas;
    }

    private void salvaRegistro(Pessoa pessoa, int linha) {
        try {
            listaDePessoas = buscaDadosDaTabela();
            FileWriter fw = new FileWriter("pooi-2019.txt");

            if (linha < 0) {
                listaDePessoas.add(pessoa);
            } else {
                listaDePessoas.set(linha, pessoa);
            }

            for (int i = 0; i < listaDePessoas.size(); i++) {
                fw.write(listaDePessoas.get(i).getNome() + ";" + listaDePessoas.get(i).getDataNascimento() + ";" + listaDePessoas.get(i).getSexo() + ";" + listaDePessoas.get(i).getTipo() + "\n");
            }

            JOptionPane.showMessageDialog(null, "Operação Efetuada com Sucesso!", "Aviso", JOptionPane.INFORMATION_MESSAGE);

            fw.close();

            //Recria a tabela para tirar seleção da linha
            containerTabela.remove(scrlPessoas);
            containerPrincipal.remove(containerTabela);
            defineTabelaPessoas(listaDePessoas);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void excluiRegistro(int linha) {
        try {
            listaDePessoas = buscaDadosDaTabela();
            FileWriter fw = new FileWriter("pooi-2019.txt");

            if (linha < 0) {
                JOptionPane.showMessageDialog(null, "Seleciona uma Linha da Tabela!", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            } else {
                listaDePessoas.remove(linha);

                for (int i = 0; i < listaDePessoas.size(); i++) {
                    fw.write(listaDePessoas.get(i).getNome() + ";" + listaDePessoas.get(i).getDataNascimento() + ";" + listaDePessoas.get(i).getSexo() + ";" + listaDePessoas.get(i).getTipo() + "\n");
                }

                JOptionPane.showMessageDialog(null, "Operação Efetuada com Sucesso!", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            }
            fw.close();

            //Recria a tabela para tirar seleção da linha
            containerTabela.remove(scrlPessoas);
            containerPrincipal.remove(containerTabela);
            defineTabelaPessoas(listaDePessoas);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void defineTabelaPessoas(List listaDePessoas) throws ParseException {
        ModeloTablePessoas modeloTabela = new ModeloTablePessoas(listaDePessoas);
        containerTabela = new JPanel();
        containerTabela.setLayout(null);
        tblPessoas = new JTable(modeloTabela);
        scrlPessoas = new JScrollPane(tblPessoas);
        containerTabela.add(scrlPessoas);
        scrlPessoas.setBounds(20, 20, 600, 200);
        containerPrincipal.add(containerTabela);
        containerTabela.setVisible(true);
        containerTabela.setSize(640, 230);
        containerTabela.setLocation(10, 210);
        containerTabela.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Clientes", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        tblPessoas.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                campoNome.setText(tblPessoas.getValueAt(tblPessoas.getSelectedRow(), 0).toString());
                campoDataNascimento.setText(tblPessoas.getValueAt(tblPessoas.getSelectedRow(), 1).toString());

                if (tblPessoas.getValueAt(tblPessoas.getSelectedRow(), 2).toString().equals("M")) {
                    buttonMasc.setSelected(true);
                } else {
                    buttonFem.setSelected(true);
                }

                if (tblPessoas.getValueAt(tblPessoas.getSelectedRow(), 3).toString().equals("SIMPLES")) {
                    campoTipo.setSelectedIndex(0);
                } else {
                    campoTipo.setSelectedIndex(1);
                }
            }
        });
    }

    public static boolean dataValida(String data) throws ParseException {
        try {
            //Verifica se a Data veio vazia.
            if (data.equals("/  /")) {
                return false;
            }

            //Verifica se a Data é válida.
            String dateFormat = "dd/MM/uuuu";
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat).withResolverStyle(ResolverStyle.STRICT);
            LocalDate.parse(data, dateTimeFormatter);

            //Verifica se a idade é maior que 18.
            Date data_atual = new Date();
            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            Date data_final = formato.parse(data);

            if ((data_atual.getTime() - data_final.getTime()) >= (325.25 * 18) * 24 * 60 * 60 * 1000) {
                return true;
            } else {
                return false;
            }
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public static void main(String[] args) throws ParseException, IOException {
        new TelaPrincipal();
    }
}
