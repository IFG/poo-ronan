package Principal;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ModeloTablePessoas extends AbstractTableModel {

    private String cabecalho[] = {"Nome", "Data de Nascimento", "Sexo", "Tipo"};
    private List<Pessoa> pessoas;

    public ModeloTablePessoas(List<Pessoa> pessoas) {
        if (pessoas != null) {
            this.pessoas = pessoas;
        } else {
            this.pessoas = null;
        }
    }

    @Override
    public int getRowCount() {
        if (pessoas != null) {
            return pessoas.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return cabecalho.length;
    }

    @Override
    public String getColumnName(int posicao) {
        return cabecalho[posicao];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return pessoas.get(rowIndex).getNome();
            case 1:
                return pessoas.get(rowIndex).getDataNascimento();
            case 2:
                return pessoas.get(rowIndex).getSexo();
            case 3:
                return pessoas.get(rowIndex).getTipo();
            default:
                break;
        }
        return null;
    }

}
