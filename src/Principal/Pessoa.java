package Principal;

public class Pessoa {

    private String nome;
    private String dataNascimento;
    private String sexo;
    private String tipo;

    public String getNome() {
        return nome;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public String getTipo() {
        return tipo;
    }

    public Pessoa(String nome, String dataNascimento, String sexo, String tipo) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.tipo = tipo;
    }

}
